/**
 * Código de implementación medicón Ecommerce enhanced Google Analytics
 * @author Ricado Montes Rodriguez <ricardomontesrodriuez@gmail.com> *
 * @constructor
 */


var currencyCode = 'COP';
var unCategorized = "Uncategorized";


var processors = {
    'impresionProduct': {
        data: function (products) { return products },
        convention: function (product) {
            return {
                'name': product.name,
                'id': product.id,
                'price': product.price,
                'brand': product.brand,
                'category': product.category,
                'variant': product.variant,
                'list': product.list,
                'position': product.position
            }
        }
    },
    'clickProduct': {
        data: function (product) { return product },
        convention: function (product) {
            return {
                'name': product.name,                      // Name or ID is required.
                'id': product.id,
                'price': product.price,
                'brand': product.brand,
                'category': product.category,
                'variant': product.variant,
                'position': product.position
            }
        }
    },
    'impresionPromo': {
        data: function (products) { return products },
        convention: function (product) {
            return {
                'name': promo.name,
                'creative': promo.creative,
                'position': promo.position
            }
        }
    },
    'clickPromo': {
        data: function (product) { return product },
        convention: function (promo) {
            return {
                'name': promo.name,
                'creative': promo.creative,
                'position': promo.position
            }

        }
    },
    'detail': {
        data: function (product) { return product },
        convention: function (product) {
            return {
                'name': product.name,
                'id': product.id,
                'price': product.price,
                'brand': product.brand,
                'category': product.category,
                'variant': product.variant,
            }
        }
    },
    'productAddToCart': {
        data: function (product) { return product },
        convention: function (product, quantity) {
            return {
                'name': product.name,
                'id': product.id,
                'price': product.price,
                'brand': product.brand,
                'category': product.category,
                'variant': product.variant,
                'quantity': quantity
            }
        }
    },

    'productRemoveToCart': {
        data: function (product) { return product },
        convention: function (product) {
            return {
                'name': product.name,
                'id': product.id,
                'price': product.price,
                'brand': product.brand,
                'category': product.category,
                'variant': product.variant,
                'quantity': product.quantity
            }
        }
    },
    'checkout': {
        data: function (products) { return products },
        convention: function (product) {
            return {
                'name': product.name,
                'id': product.id,
                'price': product.price,
                'brand': product.brand,
                'category': product.category,
                'variant': product.variant,
                'quantity': product.quantity
            }
        }
    },
    'purchase': {
        data: function (products) { return products },
        convention: function (product) {
            return {
                'name': product.name,
                'id': product.id,
                'price': product.price,
                'brand': product.brand,
                'category': product.category,
                'variant': product.variant,
                'quantity': product.quantity,
                'coupon': ''
            }
        }
    },


};



function onImpressions(products, list, _processor, category) {
    try {
        products = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](products) : products;
        var arrayProducts = []
        for (var index = 0; index < products.length; index++) {
            var product = products[index];
            product.rsListProduct = list;
            if (category) {
                product.categorie = category;
            }
            var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](product) : product;
            data.list = list;
            data.position = index + 1;
            arrayProducts.push(data);
        }
        dataLayer.push({
            'event': 'impressions',
            'ecommerce': {
                'currencyCode': currencyCode, // Local currency is optional.
                'impressions': arrayProducts
            }
        });

    } catch (error) {
        console.log(error);
    }
}

function onImpressionsPromo(promotions, _processor) {
    try {

        promotions = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](promotions) : promotions;
        var arrayPromotions = [];

        for (var index = 0; index < promotions.length; index++) {
            var promotion = promotions[index];
            var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](promotion) : promotion;
            if (data != false) {
                arrayPromotions.push(data);
            }
        }
        dataLayer.push({
            'event': 'impressionsPromo',
            'ecommerce': {
                'promoView': {
                    'promotions': arrayPromotions
                }
            }
        });

    } catch (error) {
        console.log(error);
    }
}

function onDetail(product, list, _processor) {
    try {

        product = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](product) : product;
        var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](product) : product;
        dataLayer.push({
            'event': 'detail',
            'ecommerce': {
                'detail': {
                    'actionField': { 'list': list },
                    'products': [data]
                }
            }
        });

    } catch (error) {
        console.log(error);
    }

}


function onClickProduct(product, list, _processor) {
    try {

        product = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](product) : product;
        var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](product) : product;
        sessionStorage.setItem('productList', list);
        dataLayer.push({
            'event': 'productClick',
            'ecommerce': {
                'click': {
                    'actionField': { 'list': list },
                    'products': [data]
                }
            },
            'eventCallback': function (e) {
                window.location.href = product.urlDetail;
            }
        });

    } catch (error) {
        console.log(error);
        window.location.href = product.rsUrlDetail;

    }
}


function onClickPromo(promotion, url, _processor, ) {
    try {
        promotion = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](promotion) : promotion;
        var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](promotion) : promotion;
        dataLayer.push({
            'event': 'promotionClick',
            'ecommerce': {
                'promoClick': {
                    'promotions': data != false ? [data] : []
                }
            },
            'eventCallback': function () {
                window.location.href = url;

            }
        });

    } catch (error) {
        console.log(error);
        window.location.href = url;

    }
}

function onAddToCart(product, list, quantity, _processor) {
    try {
        product = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](product, quantity) : product;
        var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](product, quantity) : product;
        dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': currencyCode,
                'add': {
                    'actionField': { 'list': list },
                    'products': [data]
                }
            }
        });

    } catch (error) {
        console.log(error);
    }


}


function onRemoveFromCart(product, list, quantity, _processor) {
    try {
        var products = [];

        function preProcess(product) {
            product = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](product, quantity) : product;
            var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](product, quantity) : product;
            return data;
        }
        if (!Array.isArray(product)) {
            products = [preProcess(product)];
        } else {
            for (var index = 0; index < product.length; index++) {
                var element = product[index];
                products.push(preProcess(element));
            }
        }

        dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'remove': {
                    'actionField': { 'list': list },
                    'products': products
                }
            }
        });

    } catch (error) {
        console.log(error);
    }


}

function onCheckout(products, _processor) {
    products = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](products) : products;
    var arrayProducts = []
    for (var index = 0; index < products.length; index++) {
        var product = products[index];
        var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](product) : product;
        arrayProducts.push(data);
    }

    dataLayer.push({
        'event': 'checkout',
        'ecommerce': {
            'checkout': {
                'actionField': { 'step': 1, 'option': 'Checkout' },
                'products': arrayProducts
            }
        }
    });

}

function onPurchase(products, total, purchase, _processor) {

    products = processors.hasOwnProperty(_processor) && processors[_processor].hasOwnProperty('data') ? processors[_processor]['data'](products) : products;
    var arrayProducts = []
    for (var index = 0; index < products.length; index++) {
        var product = products[index];
        var data = processors.hasOwnProperty(_processor) ? processors[_processor]['convention'](product) : product;
        arrayProducts.push(data);
    }

    dataLayer.push({
        'event': 'purchase',
        'ecommerce': {
            'purchase': {
                'actionField': {
                    'id': purchase.id,
                    'affiliation': 'WEB',
                    'revenue': total,
                    'coupon': purchase.coupon,
                    'shipping': '',
                    'currency': currencyCode
                },
                'products': arrayProducts
            }
        }
    });

}

