

// User id
dataLayer.push[{ 'userID': 'XXXXXXX' }];

// Inicio de sesión
dataLayer.push({ 'event': 'login', 'userId': 'XXXXXXX' });

// Inicio de sesión
dataLayer.push({ 'event': 'login_error', 'code': 'XXXXXXX' });


// Cantidad de búsquedas
dataLayer.push({ 'event': 'search', 'section': 'XXXXXXX' });

// Cotización o intención de reserva
dataLayer.push({ 'event': 'quotation_intention' });

// Reserva se debe ejecutar cuando efectivamente se realiza una reserva
dataLayer.push({ 'event': 'bookings' ,'booking_code':'XXXXXX' });

// Reserva pago se debe ejecutar cuando se realiza el pago de una reserva
dataLayer.push({ 'event': 'booking_billing','booking_code':'XXXXXX' });


// Reserva contar el número de errores c uando se genera una reserava
dataLayer.push({ 'event': 'bookings_errors', 'booking_code': 'XXXXXXX' });


// Pago reserva
dataLayer.push({ 'event': 'start_booking_payment' });

// Pago reserva
dataLayer.push({ 'event': 'booking_payment' });
